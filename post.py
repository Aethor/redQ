from __future__ import annotations
from typing import Callable, Any, List, Optional

import requests
import re
import json
import csv
import io
from bs4 import BeautifulSoup


class RedditPostAttrConstructor:
    def __init__(self, name: str, constructor: Callable[[BeautifulSoup, dict], Any]):
        self.name = name
        self.constructor = constructor

    def construct(self, page: BeautifulSoup, **kwargs) -> Any:
        return self.constructor(page, **kwargs)


class RedditPost:
    def __init__(
        self,
        url: str,
        user_agent: str,
        attribute_constructors: List[RedditPostAttrConstructor],
        **kwargs,
    ):
        r = requests.get(url, headers={"User-Agent": user_agent})
        if r.status_code != 200:
            raise Exception(f"Error downloading post : code {r.status_code}")

        soup = BeautifulSoup(r.text, "html.parser")

        for constructor in attribute_constructors:
            setattr(self, constructor.name, constructor.construct(soup, **kwargs))

    def to_json(self) -> str:
        return json.dumps(vars(self), indent=4)

    @classmethod
    def list_to_json(cls, posts: List[RedditPost]) -> str:
        return json.dumps([vars(p) for p in posts], indent=4)

    def to_xml(self) -> str:
        content = []
        for key, value in vars(self).items():
            if value is None:
                continue
            if isinstance(value, list):
                for e in value:
                    content.append(f"\t<{key[:-1]}>{str(e)}</{key[:-1]}>")
                continue
            content.append(f"\t<{key}>{str(value)}</{key}>")

        return "<post>\n" + "\n".join(content) + "\n</post>"

    @classmethod
    def list_to_xml(cls, posts: List[RedditPost]) -> str:
        content = []
        for post in posts:
            content.append("\n\t".join(post.to_xml().split("\n")))
        return "<xml>\n\t" + "\n\t".join(content) + "\n</xml>"

    def to_csv(self, **kwargs) -> str:
        """
        Convert a RedditPost to csv
        
        :param kwargs: kwargs are passed to underlying csv writer
        for fine-grained control
        """
        string_io = io.StringIO()
        writer = csv.writer(string_io, **kwargs)
        writer.writerow(vars(self).keys())
        writer.writerow(vars(self).values())
        return string_io.getvalue()

    @classmethod
    def list_to_csv(cls, posts: List[RedditPost], **kwargs) -> str:
        """
        Convert a list of RedditPost to csv
        Assume all RedditPost have the same attributes
        
        :param kwargs: is used as in RedditPost.to_csv()
        """
        if len(posts) == 0:
            return ""
        string_io = io.StringIO()
        writer = csv.writer(string_io, **kwargs)
        writer.writerow(vars(posts[0]).keys())
        for post in posts:
            writer.writerow(vars(post).values())
        return string_io.getvalue()
        


def title_constructor_fn(page: BeautifulSoup, **kwargs) -> str:
    return page.title.text.split(" : ")[0].strip()


def content_constructor_fn(page: BeautifulSoup, **kwargs) -> Optional[str]:
    content_top_node = page.find("div", id="siteTable")
    content_node = content_top_node.find("div", class_="usertext-body")
    if content_node is None:
        return None
    else:
        return re.sub("\n", "", content_node.find("div", class_="md").text)


def score_constructor_fn(page: BeautifulSoup, **kwargs) -> int:
    score_div = page.find("div", class_="score")
    return int(score_div.find("span", class_="number").text.replace(",", ""))


def comments_constructor_fn(page: BeautifulSoup, **kwargs) -> List[str]:
    """
    optional argument : max_comments_nb (default : 3)
    """
    max_comments_nb = kwargs.get("max_comments_nb", 3)

    comments = []
    comment_area_node = page.find("div", class_="commentarea")
    comment_table_node = comment_area_node.find("div", class_="sitetable")
    comment_nodes = comment_table_node.find_all(
        "div", class_="comment", limit=max_comments_nb, recursive=False
    )
    for comment_node in comment_nodes:
        comments.append(
            re.sub(
                "\n",
                "",
                comment_node.find("div", class_="entry").find("div", class_="md").text,
            )
        )
    return comments


all_attr_constructors = [
    RedditPostAttrConstructor("title", title_constructor_fn),
    RedditPostAttrConstructor("content", content_constructor_fn),
    RedditPostAttrConstructor("score", score_constructor_fn),
    RedditPostAttrConstructor("comments", comments_constructor_fn),
]
