from typing import List, Optional
import argparse
import sys

from post import RedditPost, RedditPostAttrConstructor, all_attr_constructors
from useragents import firefox_user_agent

from bs4 import BeautifulSoup
import requests


sort_types = ["relevance", "top", "comments", "new"]


def query_reddit(
    query: str,
    user_agent: str,
    max_posts_nb: int,
    sort_type: str = sort_types[0],
    attr_constructors: Optional[List[RedditPostAttrConstructor]] = None,
    **kwargs,
) -> List[RedditPost]:
    """
    Query reddit to retrieve a list of RedditPost

    :param sort_type: should be a string present in sort_types
    """
    if not sort_type in sort_types:
        raise Exception(
            f"Unknown sort type : {sort_type}. Should be one of {sort_types}"
        )

    if attr_constructors is None:
        from post import all_attr_constructors

        attr_constructors = all_attr_constructors

    r = requests.get(
        f"https://old.reddit.com/search?q={query}&sort={sort_type}",
        headers={"User-Agent": user_agent},
    )
    if r.status_code != 200:
        raise Exception(f"Error downloading search page : code {r.status_code}")

    soup = BeautifulSoup(r.text, "html.parser")

    result_nodes = soup.find_all("div", class_="search-result-link", limit=max_posts_nb)

    posts = []
    for result_node in result_nodes:
        header = result_node.find("a", class_="search-title")
        url = header["href"]
        posts.append(RedditPost(url, user_agent, attr_constructors, **kwargs))

    return posts


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument("query", type=str, help="Reddit query")
    parser.add_argument(
        "-f",
        "--format",
        type=str,
        default="json",
        help="Output format (one of 'json', 'xml' or 'csv')",
    )
    parser.add_argument(
        "-mpn",
        "--max-posts-nb",
        type=int,
        default=3,
        help="Max number of reddit posts returned",
    )
    parser.add_argument(
        "-st",
        "--sort-type",
        type=str,
        default=sort_types[0],
        help=f"Reddit sort type. Possible sort types : {sort_types}",
    )
    parser.add_argument(
        "-mcn",
        "--max-comments-nb",
        type=int,
        default=3,
        help="Max number of comments returned per post",
    )
    parser.add_argument(
        "-awl",
        "--attr-white-list",
        nargs="*",
        default=[e.name for e in all_attr_constructors],
        help="List of informations to fetch from reddit. All informations in this list will be fetched, except for those in the attr-black-list.",
    )
    parser.add_argument(
        "-abl",
        "--attr-black-list",
        nargs="*",
        default=[],
        help="Informations in this list won't be fetched. See attr-white-list default value for a list of blacklistable attributes.",
    )
    args = parser.parse_args()

    if args.query is None:
        print("No query specified. exiting...", file=sys.stderr)
        parser.print_help()
        sys.exit(1)

    if not args.sort_type in sort_types:
        print(f"Invalid sort type : {args.sort_type}")
        print(f"Hint : possible sort types are : {sort_types}")
        sys.exit(1)

    final_attr_white_list = list(set(args.attr_white_list) - set(args.attr_black_list))
    attr_constructors = [
        e for e in all_attr_constructors if e.name in final_attr_white_list
    ]

    posts = query_reddit(
        args.query,
        firefox_user_agent,
        args.max_posts_nb,
        args.sort_type,
        attr_constructors,
        max_comments_nb=args.max_comments_nb,
    )

    if args.format == "json":
        print(RedditPost.list_to_json(posts))
    elif args.format == "xml":
        print(RedditPost.list_to_xml(posts))
    elif args.format == "csv":
        print(RedditPost.list_to_csv(posts))
    else:
        print(f"Unknown format {args.format}. exiting...", file=sys.stderr)
